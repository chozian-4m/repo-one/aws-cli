ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM amazon/aws-cli:2.5.8 as cli

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY --from=cli /usr/local/aws-cli /usr/local/aws-cli

RUN groupadd -g 1001 awscli && \
    useradd -r -u 1001 -d /aws -m -s /sbin/nologin -g awscli awscli && \
    ln -s /usr/local/aws-cli/v2/current/bin/aws /usr/local/bin/aws && \
    ln -s /usr/local/aws-cli/v2/current/bin/aws_completer /usr/local/bin/aws_completer && \
    dnf upgrade -y && \
    dnf install -y groff-base less && \
    dnf clean all && \
    rm -rf /var/cache/dnf

USER 1001
WORKDIR /aws
HEALTHCHECK NONE

ENTRYPOINT ["aws"]
