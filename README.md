# aws-cli

This package provides a unified command line interface to Amazon Web Services.

## Basic Commands

An AWS CLI command has the following structure:

```
$ aws <command> <subcommand> [options and parameters]
```

For example, to list S3 buckets, the command would be:

```
$ aws s3 ls
```

To view help documentation, use one of the following:

```
$ aws help
$ aws <command> help
$ aws <command> <subcommand> help
```

To get the version of the AWS CLI:

```
$ aws --version
```

To turn on debugging output:

```
$ aws --debug <command> <subcommand>
```

## Documentation

To learn more about aws-cli [go to the complete documentation](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html).
